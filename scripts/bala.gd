extends Area2D

const JUGADOR=Color(0.11,0.43,0.87)
const ENEMIGO=Color(0.87,0.11,0.11)

export (Color) var color
var dir=Vector2()

func _ready():
	$Light2D.color=color
	connect("body_entered",self,"colision")

func _physics_process(delta):
	translate(dir*400*delta)

func colision(cuerpo):
	if cuerpo.is_in_group("enemigo"):
		cuerpo.damage(1)
	
	queue_free()