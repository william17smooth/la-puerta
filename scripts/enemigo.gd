extends KinematicBody2D

var balaR=load("res://objetos/bala.tscn")

var nav

var vida=5

var puedoDisparar=true

var velocidad=Vector2.ZERO
var ruta
var estado="patrullando"

func _ready():
	$detector.connect("body_entered",self,"encontreAlgo")
	$detector.connect("body_exited",self,"seFue")
	$temp.connect("timeout",self,"tiempo")
	nav=get_tree().get_nodes_in_group("nav")[0]
	

func _physics_process(delta):
	
	if velocidad!=Vector2.ZERO:
		$anim.play("caminando")
	else:
		$anim.play("parado")
	
	if $mira.is_colliding() and puedoDisparar:
		if $mira.get_collider().is_in_group("jugador"):
			disparar()
			puedoDisparar=false
	
	match estado:
		"patrullando":
			pass
		"te vi":
			var jugPos =get_tree().get_nodes_in_group("jugador")[0].global_position
			ruta = nav.get_simple_path(global_position,jugPos,true)
			if ruta.size() > 1 and global_position.distance_to(jugPos)>60:
				var dir = ruta[1] - global_position
				velocidad=dir.normalized()
				look_at(ruta[1])
				velocidad=move_and_slide(velocidad * 50)

func disparar():
	var bala=balaR.instance()
	bala.global_position=$spBalas.global_position
	var dir=$spBalas.global_position-global_position
	bala.color=bala.ENEMIGO
	bala.dir=dir.normalized()
	owner.add_child(bala)

func damage(d):
	vida-=d
	if vida<0:
		queue_free()

func tiempo():
	puedoDisparar=true

func encontreAlgo(cuerpo):
	if cuerpo.is_in_group("jugador"):
		estado="te vi"

func seFue(cuerpo):
	if cuerpo.is_in_group("jugador"):
		estado="patrullando"
		


func _on_vis_screen_exited():
	set_physics_process(false)
	


func _on_vis_screen_entered():
	set_physics_process(true)
