extends KinematicBody2D

var balaR=load("res://objetos/bala.tscn")

var vida=50

var destino=Vector2()
var ruta=[]
var estado="esperando"

var velocidad=Vector2.ZERO

var nav
var line

var puedoDisparar=true
var vel=100

func _ready():
	nav=get_tree().get_nodes_in_group("nav")[0]
	line=get_tree().get_nodes_in_group("line")[0]
	

func _physics_process(delta):
	
	look_at(get_global_mouse_position())
	
	match estado:
		"esperando":
			velocidad=Vector2.ZERO
			if Input.is_mouse_button_pressed(BUTTON_LEFT):
				destino=get_global_mouse_position()

				estado="caminando"
		"caminando":
			if Input.is_mouse_button_pressed(BUTTON_LEFT):
				
				destino=get_global_mouse_position()
			
			ruta=nav.get_simple_path(global_position,destino,false)
			line.points=ruta
			
			if ruta.size()>1: 
				var dir=ruta[1]-global_position
				if global_position.distance_to(destino)<5:
					estado="esperando"
					
				
				velocidad=dir.normalized()
				
				velocidad=move_and_slide(velocidad*vel)
			
	if $mira.is_colliding():
		if $mira.get_collider().is_in_group("enemigo") and puedoDisparar:
			puedoDisparar=false
			var bala=balaR.instance()
			var direccion=$arma/spBalas.global_position-global_position
			bala.dir=direccion.normalized()
			bala.global_position=$arma/spBalas.global_position
			bala.color=bala.JUGADOR
			owner.add_child(bala)
	
	
	if velocidad != Vector2.ZERO:
		$anim.play("caminar")
	else:
		$anim.play("parado")
	
func damage(d):
	vida-=d
	if vida<0:
		get_tree().reload_current_scene() 


func _on_temp_timeout():
	puedoDisparar=true
